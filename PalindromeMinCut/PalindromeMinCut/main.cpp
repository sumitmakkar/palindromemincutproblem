#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        string str;
    
        vector< vector<bool> > populateMatrixVector()
        {
            vector< vector<bool> > matrixVector;
            int                    len = (int)str.length();
            for(int i = 0 ; i < len ; i++)
            {
                vector<bool> innerVector;
                for(int j = 0 ; j < len ; j++)
                {
                    innerVector.push_back(false);
                }
                matrixVector.push_back(innerVector);
            }
            return matrixVector;
        }
    
        void display(vector< vector<bool> > matrixVector)
        {
            int len = (int)str.length();
            for(int i = 0 ; i < len ; i++)
            {
                for(int j = 0 ; j < len ; j++)
                {
                    cout<<matrixVector[i][j]<<" ";
                }
                cout<<endl;
            }
        }
    
        void findMinimumCuts(vector< vector<bool> > matrixVector)
        {
            int         len = (int)str.length();
            vector<int> cutsVector(len);
            for(int i = 0 ; i < len ; i++)
            {
                int temp = INT_MAX;
                if(matrixVector[0][i])
                {
                    cutsVector[i] = 0;
                }
                else
                {
                    for(int j = 0 ; j < i ; j++)
                    {
                        if(matrixVector[j+1][i] && temp > cutsVector[j] + 1)
                        {
                            temp = cutsVector[j] + 1;
                        }
                    }
                    cutsVector[i] = temp;
                }
            }
            cout<<cutsVector[len-1]<<endl;
        }
    
        void replaceMatrixValuesWithNewPalindromeValues()
        {
            vector< vector<bool> > matrixVector = populateMatrixVector();
            int len = (int)str.length();
            for(int currLevel = 1 ; currLevel <= len ; currLevel++)
            {
                for(int i = 0 ; i <= len - currLevel ; i++)
                {
                    int j = i + currLevel - 1;
                    if(str[i] == str[j])
                    {
                        if(currLevel > 1)
                        {
                            if(matrixVector[i+1][j-1])
                            {
                                matrixVector[i][j] = true;
                            }
                        }
                        else
                        {
                            matrixVector[i][j] = true;
                        }
                    }
                }
            }
            //display(matrixVector);
            findMinimumCuts(matrixVector);
        }
    
    public:
        Engine(string s)
        {
            str = s;
            replaceMatrixValuesWithNewPalindromeValues();
        }
};

int main(int argc, const char * argv[])
{
//    string str = "79812345654321";
//    string str = "BABCDCB";
    string str = "bananna";
    Engine e   = Engine(str);
    return 0;
}
